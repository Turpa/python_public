import sys
import random
import math
from dataclasses import dataclass
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib, time
from mpl_toolkits.mplot3d import Axes3D



@dataclass
class Vertex:
    id_corner:float
    x:float
    y:float
    z:float


class Cube:
    def __init__(self, center, size):
        self.yaw_degree_ange = 0
        self.pitch_degree_ange = 0
        self.center = center
        self.size = size
        self.signs = np.array([[1, 1, 1], [-1, 1, 1], [-1, -1, 1], [1, -1, 1], 
        [1, 1, -1], [-1, 1, -1], [-1, -1, -1], [1, -1, -1]])
        self.vertexes = []
        self.id_corner = 1.1
        for i in range(8):
            x, y, z = self.center + self.size * self.signs[i] / 2
            id_corner = self.id_corner
            vertex = Vertex(id_corner, x, y, z)
            self.vertexes.append(vertex)
            self.id_corner = float("{:.1f}".format(self.id_corner + 0.1))
            if(self.id_corner == 1.5):
                self.id_corner = 2.10

    def draw(self):     
        self.rectangle_top_x, self.rectangle_top_y, self.rectangle_top_z = [], [], []
        self.rectangle_bottom_x, self.rectangle_bottom_y, self.rectangle_bottom_z = [], [], []
        self.rectangle_corner1_x, self.rectangle_corner1_y, self.rectangle_corner1_z = [], [], []
        self.rectangle_corner2_x, self.rectangle_corner2_y, self.rectangle_corner2_z = [], [], []
        self.rectangle_corner3_x, self.rectangle_corner3_y, self.rectangle_corner3_z = [], [], []
        self.rectangle_corner4_x, self.rectangle_corner4_y, self.rectangle_corner4_z = [], [], []

        for vertex in self.vertexes:
            if (vertex.id_corner < 2):
                self.rectangle_top_x.append(vertex.x)
                self.rectangle_top_y.append(vertex.y)
                self.rectangle_top_z.append(vertex.z)
            if (vertex.id_corner > 2):
                self.rectangle_bottom_z.append(vertex.z)
                self.rectangle_bottom_y.append(vertex.y)
                self.rectangle_bottom_x.append(vertex.x)
            if (vertex.id_corner == 1.1 or vertex.id_corner == 2.1 ):
                self.rectangle_corner1_x.append(vertex.x)
                self.rectangle_corner1_y.append(vertex.y)
                self.rectangle_corner1_z.append(vertex.z)
            if (vertex.id_corner==1.2 or vertex.id_corner==2.2 ):
                self.rectangle_corner2_x.append(vertex.x)
                self.rectangle_corner2_y.append(vertex.y)
                self.rectangle_corner2_z.append(vertex.z)
            if (vertex.id_corner==1.3 or vertex.id_corner==2.3 ):
                self.rectangle_corner3_x.append(vertex.x)
                self.rectangle_corner3_y.append(vertex.y)
                self.rectangle_corner3_z.append(vertex.z)
            if (vertex.id_corner==1.4 or vertex.id_corner==2.4 ):
                self.rectangle_corner4_x.append(vertex.x)
                self.rectangle_corner4_y.append(vertex.y)
                self.rectangle_corner4_z.append(vertex.z)
        
        for item in [self.rectangle_top_x, self.rectangle_top_y, self.rectangle_top_z,
                    self.rectangle_bottom_x, self.rectangle_bottom_y, self.rectangle_bottom_z]:   
            item.append(item[0])
        
        ax = set_axes()
    
        ax.plot(self.rectangle_top_x, self.rectangle_top_y, self.rectangle_top_z, 'm') 
        ax.plot(self.rectangle_bottom_x, self.rectangle_bottom_y, self.rectangle_bottom_z, 'c') 
        ax.plot(self.rectangle_corner1_x, self.rectangle_corner1_y, self.rectangle_corner1_z, 'k')
        ax.plot(self.rectangle_corner2_x, self.rectangle_corner2_y, self.rectangle_corner2_z, 'g')
        ax.plot(self.rectangle_corner3_x, self.rectangle_corner3_y, self.rectangle_corner3_z, 'r')
        ax.plot(self.rectangle_corner4_x, self.rectangle_corner4_y, self.rectangle_corner4_z, 'y')
        fig.canvas.draw()
        plt.show()

    def rotate_pitch(self):
        r = np.zeros([3,3])
        beta = np.deg2rad(self.pitch_degree_ange)
        r[1:2,1:2] = [1]
        r[0:1,0:1], r[2:3,2:3] = [np.cos(beta)], [np.cos(beta)]
        r[0:1,2:3], r[2:3,0:1] = [np.sin(beta)], [-np.sin(beta)]
        
        for index, vertex in  enumerate(self.vertexes):
            pt = np.matmul(r, np.array([vertex.x, vertex.y, vertex.z ]))
            vertex_rotated = Vertex(vertex.id_corner, pt[0], pt[1], pt[2])
            self.vertexes[index] = vertex_rotated

        self.draw()

    def rotate_yaw(self):
        r = np.zeros([3,3])
        alpha = np.deg2rad(self.yaw_degree_ange)
        # beta =
        # gama
        
        r[:1,0:1], r[1:2,1:2] = [np.cos(alpha)],  [np.cos(alpha)]
        r[:1,1:2], r[1:2,:1] = [-np.sin(alpha)], [np.sin(alpha)]
        r[2:3,2:3] = [1]

        for index, vortex in enumerate(self.vertexes):
            pt = np.matmul(r, np.array([vortex.x, vortex.y, vortex.z]))
            vertex_rotated = Vertex(vortex.id_corner, pt[0], pt[1], pt[2])
            self.vertexes[index] = vertex_rotated

        self.draw()

    @staticmethod
    def normalizeAngle(angle):
        newAngle = angle % 360
        # if angle < 0:
        #     newAngle = angle % 360
        # elif angle > 359:
        #     newAngle = angle % 360
        return newAngle

    def left_keypress(self):
        self.yaw_degree_ange = self.normalizeAngle(self.yaw_degree_ange - 1)
        self.rotate_yaw()
    def right_keypress(self):
        self.yaw_degree_ange = self.normalizeAngle(self.yaw_degree_ange + 1)
        self.rotate_yaw()
    def up_keypress(self):
        self.pitch_degree_ange = self.normalizeAngle(self.pitch_degree_ange + 1)
        self.rotate_pitch()
    def down_keypress(self):
        self.pitch_degree_ange = self.normalizeAngle(self.pitch_degree_ange - 1)
        self.rotate_pitch()

def set_axes():
    plt.clf()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_title('Press Enter or x')
    # Turn off autoscale.
    ax.autoscale(enable=False,axis='both')  #you will need this line to change the Z-axis
    
    # Bound axis to meet in center.
    ax.set_xbound(-10, 10)
    ax.set_ybound(-10, 10)
    ax.set_zbound(-10, 10)
    # Hide grid lines
    ax.grid(False)
    
    # Hide axes ticks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    plt.axis('off')
    plt.grid(b=None)
    return ax

def press(event):
    sys.stdout.flush()
    print(event.key)
    if event.key == 'left':
        cube.left_keypress()   
    if event.key == 'right':
        cube.right_keypress()
    if event.key == 'up':
        cube.up_keypress()
    if event.key == 'down':
        cube.down_keypress()

fig = plt.figure()
fig.canvas.mpl_connect('key_press_event', press)
# plt.ion()
center = np.array([0, 0, 0])
size = 5
cube = Cube(center, size)

cube.draw()
# plt.draw()
# plt.show(block = False)
# plt.show()



